from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from django.urls import reverse
from django.http import HttpResponseRedirect

# Create your views here.
def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists,
    }
    return render(request, 'todos/todo_list_list.html', context)


def todo_item_list(request):
    todo_items = TodoItem.objects.all()
    context = {
        'todo_items': todo_items,
    }
    return render(request, 'todos/todo_item_list.html', context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    todoitems = todolist.items.all()  # This is the correct usage based on your related_name in models
    context = {'todolist': todolist, 'todoitems': todoitems}
    return render(request, 'todos/todo_list_detail.html', context)


def todo_list_create(request):
    if request.method == "POST":
        name = request.POST.get('name')
        if name:
            todo_list = TodoList(name=name)
            new_form = todo_list.save()
            return redirect("todo_list_detail", id=new_form.id)
    return render(request, 'todos/todo_list_create.html')


def todo_list_update(request):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)

    # To add something to the model, like setting a user,
    # use something like this:
    #
    # model_instance = form.save(commit=False)
    # model_instance.user = request.user
    # model_instance.save()
    # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoList(instance=todo_list)

    context = {
        "form": form
    }

    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_detail", id=todo_list.id)

    return render(request, "todos/todo_list_delete.html")


def todo_list_item_create(request):
    # todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.item_create()
        return redirect("todo_list_detail", id=todo_list.id)

    return render(request, "todos/todo_item_create.html")
