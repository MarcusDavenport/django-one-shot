from django.urls import path, include
from todos import views

urlpatterns = [
    path('todolists/', views.todo_list_list, name='todo_list_list'),
    path('', views.todo_list_list, name='todo_list_list'),
    path('<int:id>/', views.todo_list_detail, name='todo_list_detail'),
    path('create/', views.todo_list_create, name='todo_list_create'),
    path('<int:id>/edit/', views.todo_list_update, name='todo_list_update'),
    path('<int:id>/delete/', views.todo_list_delete, name='todo_list_delete'),
    path('item/create/', views.todo_list_item_create, name='todo_list_item_create'),
]
