from django.forms import ModelForm
from todos.models import TodoItem


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ['task', 'due_date', 'is_completed', 'list']
        # widgets = {
        #     'task': forms.TextInput(attrs={'type': 'text'}),
        #     'due_date': forms.TextInput(attrs={'type': 'text'}),
        #     'is_completed': forms.CheckboxInput(attrs={'type': 'checkbox'}),
        #     'list': forms.Select(attrs={'name': 'list'})
        # }
