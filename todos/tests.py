from django.test import TestCase
from django.urls import reverse
from .models import TodoList, TodoItem

# Create your tests here.


class TodoListTestCase(TestCase):
    def setUp(self):
        TodoList.objects.create(name="Shopping List")

    def test_todolist_name(self):
        todolist = TodoList.objects.get(id=1)
        self.assertEqual(todolist.name, 'Shopping List')


class TodoItemTestCase(TestCase):
    def setUp(self):
        todolist = TodoList.objects.create(name="Shopping List")
        TodoItem.objects.create(task="Buy milk", list=todolist)

    def test_todoitem_task(self):
        todoitem = TodoItem.objects.get(id=1)
        self.assertEqual(todoitem.task, 'Buy milk')

    def test_todolist_view(self):
        response = self.client.get(reverse('todo_list_list'))
        self.assertEqual(response.status_code, 200)

    def test_todoitem_view(self):
        response = self.client.get(reverse('todo_item_list'))
        self.assertEqual(response.status_code, 200)
